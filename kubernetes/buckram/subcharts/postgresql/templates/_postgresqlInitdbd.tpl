{{/* vim: set filetype=mustache: */}}
{{/*
Script for doing final setup after initdb
*/}}
{{- define "postgresqlInitdbd" -}}

{{- if .Values.security.setAllMd5 -}}

sed -i 's/^\([^#].*\)trust$/\1md5/g' $PGDATA/pg_hba.conf
{{- end -}}

{{- end -}}
