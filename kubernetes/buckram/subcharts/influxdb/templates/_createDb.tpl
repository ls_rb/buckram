{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "create-db-query" -}}
{{- $duration := default "0d" .Values.createDatabase.database.duration -}}
{{- $database := default "prometheus" .Values.createDatabase.database.database -}}
{{- $shardDuration := default "7d" .Values.createDatabase.database.shardDuration -}}
{{- $retentionPolicyName := default "autogen" .Values.createDatabase.database.retentionPolicyName -}}
CREATE DATABASE {{ $database }} WITH DURATION {{ $duration }} REPLICATION 1 SHARD DURATION {{ $shardDuration }} NAME {{ $retentionPolicyName }}
{{- end -}}
